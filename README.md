Poirot
======

Rastrear y cruzar información para deducir datos. Funciona con archivo `.txt`, `.pdf`, `.html`, `.doc`, `odt`, etc

Instalación
-----------

~~~
wget https://raw.github.com/b4zz4/Poirot/master/poirot
chmod +x poirot
cp poirot /usr/bin/
~~~

Uso
---

1. Guarda toda la información en un directorio permanece siempre en el para trabajar.
2. Correr `poirot` o `poirot -z` para la interfaz grafica y escribir una lista de palabras claves.
3. La salida van a ser la lista de archivos coincidentes con todas ellas, esos archivos van a contener información importante sobre el tema que estas buscado.

_**Nota:** también se pueden buscar los archivos en `/tmp` para hacer otras búsquedas_


Pendientes
----------

* Versión más inteligente
  * Varios tipos de cruces
  * Coincidencias entre archivos (simples y detectivescas)
    * Convertir con markdown, quitar datos repetidos
